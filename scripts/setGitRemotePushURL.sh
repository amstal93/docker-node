#!/usr/bin/env bash

set -e

: "${CI_REPOSITORY_URL?Need to set CI_REPOSITORY_URL}"

# Transforms the repository URL to the SSH URL
# Example input: https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/malfter/ci-examples.git
# Example output: git@gitlab.com/malfter/ci-examples.git
CI_REPOSITORY_URL_SSH=$(echo "${CI_REPOSITORY_URL}"  | cut -d'@' -f2 | sed 's#/#:#' | sed 's/^/git@/')

git remote set-url --push origin "${CI_REPOSITORY_URL_SSH}"
git fetch --all